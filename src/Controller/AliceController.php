<?php

/**
 * @file
 * Contains \Drupal\alice\Controller\AliceController.
 */

namespace Drupal\alice\Controller;

use Drupal\Core\Url;
use Drupal\Core\Controller\ControllerBase;
use Drupal\alice\AliceFixtures;

/**
 * Display table of module fixture yaml files.
 */
class AliceController extends ControllerBase {

  /**
   * Manage entity fixture data.
   */
  public function fixtureOverview() {
    $alice = new \Drupal\alice\AliceFinder;
    $rows = $alice->findAllFixtures();

    $form = \Drupal::formBuilder()->getForm('Drupal\alice\Form\AliceFixturesDisplayForm', $rows);

    /*
    // List module name with all associated fixture files.
    // We need the details of the fixture file, status and what for.
    // [horizontal tabs][table select]
    
    $query = db_select('backup_db', 'e');
    $count_query = clone $query;
    $count_query->addExpression('Count(e.eid)');

    $paged_query = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender');
    // @todo, limit should be managed.
    $paged_query->limit(10);
    $paged_query->setCountQuery($count_query);

    $results = $paged_query
      ->fields('e', array('fid', 'name', 'uri', 'created'))
      ->orderBy('created', 'DESC')
      ->execute()
      ->fetchAll();

    $rows = array();
    foreach ($results as $result) {
      $url = backup_db_link($result->uri);
      $location = \Drupal::l($result->uri, Url::fromUri(file_create_url($url)));
      $created = \Drupal::service('date.formatter')->format($result->created, 'html_date');

      $rows[$result->fid] = array(
        'fid' => $result->fid,
        'name' => $result->name,
        'location' => $location,
        'created' => $created
      );
    }*/

    return array(
      'form' => $form,
      'pager' => array(
        '#type' => 'pager',
        '#weight' => 5,
      )
    );
  }
}
