<?php

/**
 * @file
 * \Drupal\alice\Event\ImportFixtureEvent
 */

namespace Drupal\alice\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Import fixture event class.
 */
class ImportFixtureEvent extends Event {

  protected $data;

  /**
   * Constructor
   *
   * @param Entity $data
   */
  public function __construct($data) {
    $this->data = $data;
  }

  /**
   * Getter for Entity data
   *
   * @return Entity
   */
  public function getData() {
    return $this->data;
  }

  /**
   * Setter for Entity data
   *
   * @param Entity
   */
  public function setData($data) {
    $this->data = $data;
  }
}
