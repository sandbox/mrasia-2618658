<?php

/**
 * @file
 * \Drupal\alice\AliceEvents
 *
 * @see Acme\StoreBundle\Event\FilterOrderEvent
 */

namespace Drupal\alice;

final class AliceEvents {

  /**
   * The alice.import event is thrown on every
   * fixture import.
   *
   * The event listener receives an
   * -_-
   *
   * @var string
   */
  const CREATE = 'alice.import.create';

  /**
   *
   * @var string
   */
  const SAVE = 'alice.import.save';
}



















