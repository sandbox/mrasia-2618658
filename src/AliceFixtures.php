<?php

/**
 * @file
 * \Drupal\alice\AliceFixtures
 */

namespace Drupal\alice;

use Nelmio\Alice;
use Drupal\alice\AliceEvents;
use Drupal\alice\Event\ImportFixtureEvent;
use Drupal\alice\DataFixtures\AliceProcessor;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

// @todo, http://www.doctrine-project.org/api/orm/2.0/source-class-Doctrine.ORM.EntityManager.html#32-751
// https://api.drupal.org/api/drupal/vendor%21doctrine%21common%21lib%21Doctrine%21Common%21Persistence%21ObjectManager.php/8
// use Doctrine\Common\Persistence\ObjectManager;

class AliceFixtures implements AliceFixturesInterface {

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Constructor
   */
  public function __construct(EventDispatcherInterface $event_dispatcher, AliceProcessor $processor) {
    $this->eventDispatcher = $event_dispatcher;

    // Testing.
    $this->processor = new AliceProcessor();
    // $this->processor = $processor;
  }

  /**
   * {@inheritdoc}
   */
  public function loadFixture(array $fixtures) {
    $loader = new \Nelmio\Alice\Fixtures\Loader();

    try {
      $objects = array();
      foreach ($fixtures as $fixture => $weight) {
        $objects = array_merge($objects, $loader->load($fixture));
      }
    }
    catch (\Exception $e) {
      \Drupal::logger('alice')->error('Could not load Fixture(s), @error', 
        array(
          '@error' => $e->getMessage()
        )
      );
    }

    // Part of the OB tests - see below.
    // $objects = $loader->load('modules/alice/config/fixtures/entity/nodes.yml');
    // $objects = array_merge($objects, $loader->load('modules/alice/config/fixtures/entity/nodes_entity_example.yml'));

    // @todo, inject our own implementation of ObjectManager... ='(
    // $persister = new \Nelmio\Alice\Persister\Doctrine($om);
    // $persister->persist($objects);

    // @todo, processors should be added via a service.
    // @todo, MethodInterface -_-, as above (OB).
    // $loader->addProcessor($this->processor->preProcess($objects));

    // Dispatch update entities event.
    $event = new ImportFixtureEvent($objects);
    $this->eventDispatcher->dispatch(AliceEvents::SAVE, $event);
  }

  /**
   * {@inheritdoc}
   */
  public function resetFixture($uri) {
    
  }

  /**
   * {@inheritdoc}
   */
  public function purgeFixture($uri) {
    // Look whos purging now -_'-
  }

}
