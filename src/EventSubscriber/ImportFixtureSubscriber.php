<?php

/**
 * @file
 * \Drupal\alice\EventSubscriber\ImportFixtureSubscriber
 */

namespace Drupal\alice\EventSubscriber;

use Drupal\alice\AliceEntity;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Import fixture data subscriber class.
 */
class ImportFixtureSubscriber implements EventSubscriberInterface {

  public static function getSubscribedEvents() {
    return array(
      'alice.import.create' => array('onImportCreate', 0),
      'alice.import.save' => array('onImportSave', 5)
    );
  }

  /**
   * @todo, not in use.
   */
  public function onImportCreate($data) {
    // Create the requested entities.
    $entityHandler = new AliceEntity();

    foreach ($data->getData() as $entity) {
      // Create, save entity.
      $entityHandler->create($entity);
    }
  }

  /**
   * Updates entity after initial creation.
   */
  public function onImportSave($data) {    
    $entityHandler = new AliceEntity();

    foreach ($data->getData() as $entity) {
      // Associate field data.
      $entityHandler->fields($entity);
      $entityHandler->update($entity);
    }

    // Track the installed fixture data.
    // $entityHandler->track();
  }

}
