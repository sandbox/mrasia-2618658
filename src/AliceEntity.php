<?php

/**
 * @file
 * \Drupal\alice\AliceEntity
 */

namespace Drupal\alice;

/**
 * AliceEntity class.
 */
class AliceEntity {

  /**
   * Update existing Entity.
   */
  public function update($entity) {
    // Load the placeholder entity.
    $placeholder = $entity->entity;
    unset($entity->entity);

    // Update values provided by the data fixture.
    $values = get_object_vars($entity);
    foreach ($values as $key => $value) {
      if (!empty($value)) {
        $placeholder->{$key} = $value;
      }
    }

    // Save the now complete entity.
    $placeholder->save();
  }

  /**
   * Format field data.
   */
  public function fields($entity) {
    foreach ($entity->field_data as $name => $value) {
      $entity->{$name} = $value;
    }

    unset($entity->field_data);
  }

  /**
   * Track data created by fixture.
   */
  public function track() {
    // @todo, track created entity.
  }

  /**
   * Remove data created by fixture.
   */
  public function remove() {
    // @todo, remove the fixture data.
    // Called from purge.
  }

}




