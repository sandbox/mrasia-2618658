<?php

/**
 * @file
 * \Drupal\alice\AliceFinder
 */

namespace Drupal\alice;

class AliceFinder {

  /**
   * {@inheritdoc}
   */
  public function findAllFixtures() {
    $moduleHandler = \Drupal::moduleHandler();
    $modules = $moduleHandler->getModuleList();

    foreach ($modules as $name => $module) {
      if ($fixture = $this->findFixture($name)) {
        $files[$name] = array_values($fixture);
      }
    }

    return $files;
  }

  /**
   * {@inheritdoc}
   */
  public function findFixture($module) {
    return file_scan_directory(drupal_get_path('module', $module). '/config/fixtures', '/\.yml/');
  }
}
