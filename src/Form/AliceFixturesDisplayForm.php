<?php

/**
 * @file
 * Contains \Drupal\alice\Form\AliceFixturesDisplayForm.
 */

namespace Drupal\alice\Form;

// use Drupal\alice\AliceFixtures;
use Drupal\Core\Form\FormStateInterface;

/**
 * AliceFixturesDisplayForm class.
 */
class AliceFixturesDisplayForm extends AliceFixturesFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alice_fixtures_display_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $results = array()) {
    $form = parent::buildForm($form, $form_state);

    $header = array(
      'name' => $this->t('Name'),
      'status' => $this->t('Status'),
    );

    $form['#attached']['library'][] = 'alice/alice.admin';
    $form['table'] = array(
      '#type' => 'table',
      '#header' => $header,
      '#empty' => t('No fixtures found.')
    );

    $modules = array();
    foreach ($results as $module => $files) {
      $modules[] = $module;

      $form['table'][$module]['module'] = array(
        '#wrapper_attributes' => array(
          'colspan' => 2,
          'class' => array('module'),
          'id' => 'module-' . $module,
        ),
        '#markup' => $module
      );

      foreach ($files as $file) {
        $form['table'][$file->uri]['name'] = array(
          '#title' => $this->t('@name', array('@name' => $file->name)),
          '#wrapper_attributes' => array(
            'class' => array('checkbox'),
          ),
          '#type' => 'checkbox',
          '#default_value' => 0,
          '#parents' => array($module, $file->uri)
        );
        $form['table'][$file->uri]['status'] = array(
          '#markup' => 'Not Installed'
        );
      }
    }

    $form['modules'] = array(
      '#type' => 'value',
      '#value' => $modules,
    );
    $form['actions']['submit']['#value'] = $this->t('Next');

    return $form;
  }

  /**
   * @todo, validate all selected fixtures are uninstalled first.
   * makes data more managable - alert user they can create new fixture file with same contents.
   */
  

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $fixtures = array();
    foreach ($form_state->getValue('modules') as $module) {
      $fixtures[$module] = array_filter($form_state->getValue($module));
    }

    $this->store->set('fixtures', $fixtures);
    $form_state->setRedirect('alice.manager.order');
  }
}
