<?php

/**
 * @file
 * Contains \Drupal\alice\Form\AliceFixturesOrderForm.
 */

namespace Drupal\alice\Form;

use Drupal\alice\AliceFixtures;
use Drupal\Core\Form\FormStateInterface;

/**
 * AliceFixturesDisplayForm class.
 */
class AliceFixturesOrderForm extends AliceFixturesFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'alice_fixtures_order_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['instructions'] = array(
      '#type' => 'item',
      '#title' => $this->t('Fixture dependency'),
      '#prefix' => '<p>',
      '#markup' => $this->t('Fixtures that inherit templates or other dependency related stuff must be ordered correctly.'),
      '#suffix' => '</p>'
    );

    $header = array(
      'name' => $this->t('Name'),
      'module' => $this->t('Module'),
      'weight' => $this->t('Order')
    );

    $form['table'] = array(
      '#type' => 'table',
      '#header' => $header,
      '#empty' => t('No fixtures selected.'),
      '#attributes' => array('id' => 'fixture-order-table'),
      '#tabledrag' => array(
        array(
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'fixture-item-weight',
        ),
      )
    );

    $fixtures = $this->store->get('fixtures') ? $this->store->get('fixtures') : [];
    foreach ($fixtures as $module => $values) {
      foreach ($values as $fixture => $value) {
        $form['table'][$fixture]['#attributes']['class'][] = 'draggable';
        $form['table'][$fixture]['name'] = array(
          '#plain_text' => $fixture,
        );
        $form['table'][$fixture]['module'] = array(
          '#plain_text' => $module,
        );
        $form['table'][$fixture]['weight'] = array(
          '#type' => 'weight',
          '#title' => t('Weight for @title', array('@title' => $fixture)),
          '#title_display' => 'invisible',
          '#attributes' => array('class' => array('fixture-item-weight')),
        );
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Load fixtures for processing.
    $alice = \Drupal::service('alice.import.create');
    $alice->loadFixture($form_state->getValue('table'));

    // Clear the temp storage.
    $this->deleteStore('fixtures');

    // Redirect to manager.
    $form_state->setRedirect('alice.manager');

    // @todo
    drupal_set_message(t('Yay =D'), 'status');
  }
}
