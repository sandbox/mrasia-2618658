<?php

/**
 * @file
 * \Drupal\alice\DataFixtures\AliceProcessor
 */

namespace Drupal\alice\DataFixtures;

use Nelmio\Alice\ProcessorInterface;

class AliceProcessor implements ProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function preProcess($objects) {
    // print_r($objects);
    // exit;
  }

  /**
   * {@inheritdoc}
   */
  public function postProcess($objects) {
    
  }
}
