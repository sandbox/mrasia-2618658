<?php

/**
 * @file
 * \Drupal\alice\DataFixtures\Entity\Node
 */

namespace Drupal\alice\DataFixtures\Entity;

/**
 * Defines a Node.
 */
class Node extends EntityBase {

  /**
   * Node bundle
   */
  public $bundle;

  /**
   * Entity type
   */
  public $type;

  /**
   * Node title
   */
  public $title;

  /**
   * Node langcode 
   */
  public $langcode;

  /**
   * Node status
   */
  public $status;

  /**
   * Node author
   */
  public $uid;

}
