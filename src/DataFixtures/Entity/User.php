<?php

/**
 * @file
 * \Drupal\alice\DataFixtures\Entity\User
 */

namespace Drupal\alice\DataFixtures\Entity;

/**
 * Defines a User.
 */
class User extends EntityBase {

  /**
   *
   */
  public $username;

  /**
   *
   */
  public $password;

  /**
   *
   */
  public $email;

  /**
   *
   */
  public $status;

  /**
   *
   */
  public $roles;

  /**
   *
   */
  public $field_data;
}
