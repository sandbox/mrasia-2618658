<?php

/**
 * @file
 * \Drupal\alice\DataFixtures\Entity\Taxonomy
 */

namespace Drupal\alice\DataFixtures\Entity;

/**
 * Defines a Taxonomy term.
 */
class Taxonomy extends EntityBase {

  /**
   * 
   */
  public $name;
  
  /**
   *
   */
  public $type;

}
