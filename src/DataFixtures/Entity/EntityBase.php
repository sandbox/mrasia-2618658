<?php

/**
 * @file
 * \Drupal\alice\DataFixtures\Entity\EntityBase
 */

namespace Drupal\alice\DataFixtures\Entity;

/**
 * Defines a generic Entity base.
 */
class EntityBase {

  /**
   * Entity id.
   */
  protected $id;

  /**
   * Entity field data.
   */
  public $field_data;

  /**
   * Constructor, create entity placeholder.
   */
  public function __construct($bundle, $values) {
    $this->entity = \Drupal::entityManager()
      ->getStorage($bundle)
      ->create((array) $values);

    // Save entity.
    $this->entity->save();
  }

  /**
   * Return Entity id.
   */
  public function getId() {
    return $this->entity->id();
  }

}
