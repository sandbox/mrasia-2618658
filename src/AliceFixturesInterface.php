<?php

/**
 * @file
 * \Drupal\alice\AliceFixturesInterface
 */

namespace Drupal\alice;

/**
 * Describes an interface for Alice fixtures.
 *
 * @todo, https://github.com/hautelook/AliceBundle/blob/master/src/Alice/DataFixtures/LoaderInterface.php
 * all the processors thing.
 */
interface AliceFixturesInterface {

  /**
   * Load fixture(s) from uri.
   *
   * @param $fixtures array
   */
  public function loadFixture(array $fixtures);

  /**
   * Reset fixture data.
   */
  public function resetFixture($uri);

  /**
   * Remove fixture data.
   */
  public function purgeFixture($uri);

}
